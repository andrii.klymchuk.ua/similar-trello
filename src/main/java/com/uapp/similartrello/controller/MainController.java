package com.uapp.similartrello.controller;

import com.uapp.similartrello.exception.ValidateException;
import com.uapp.similartrello.form.RegistrationForm;
import com.uapp.similartrello.model.Group;
import com.uapp.similartrello.model.Task;
import com.uapp.similartrello.model.User;
import com.uapp.similartrello.model.UserRole;
import com.uapp.similartrello.service.UserService;
import com.uapp.similartrello.service.logic.GroupingTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@Api("Main controller")
public class MainController {

    private final GroupingTaskService groupingTaskService;
    private final UserService userService;

    public MainController(GroupingTaskService groupingTaskService, UserService userService) {
        this.groupingTaskService = groupingTaskService;
        this.userService = userService;
    }

    @GetMapping("/")
    @ApiOperation("Main page")
    public ModelAndView index() {
        final String username = getAuthenticationUserName();
        final Map<Group, List<Task>> groups = getAuthenticationUserName().isEmpty()
                ? Collections.emptyMap()
                : groupingTaskService.grouping4User(username);

        return new ModelAndView("index")
                .addObject("groups", groupingTaskService.getSortedGroupingTaskMap(groups));
    }

    @RequestMapping("login")
    @ApiOperation("Login page")
    public ModelAndView login() {
        return new ModelAndView("login");
    }

    @GetMapping("registration")
    @ApiOperation("Registration main page")
    public ModelAndView initRegistration() {
        return new ModelAndView("registration");
    }

    @PostMapping("registration")
    @ApiOperation("Registration post method")
    public ModelAndView registration(@ModelAttribute("form") @Valid RegistrationForm form, BindingResult bindingResult) {
        final ModelAndView model = new ModelAndView("registration");

        if (bindingResult.hasErrors()) {
            return model.addObject("error", ValidateException.getStringErrors(bindingResult.getAllErrors()));
        }

        if (!form.getPassword().equals(form.getRepeat())) {
            return model.addObject("error", "Password and repeat not equals");
        }

        try {
            userService.save(getUser(form));
        } catch (Exception e) {
            return model.addObject("error", "User can`t be save!");
        }

        return model.addObject("message", "User create!");
    }

    private String getAuthenticationUserName() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        final Object principal = authentication == null
                ? null
                : authentication.getPrincipal();

        return !(principal instanceof UserDetails)
                ? "" //principal.toString()
                : ((UserDetails) principal).getUsername();
    }

    private User getUser(RegistrationForm form) {
        final User user = new User();
        user.setUsername(form.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setEnabled(true);
        user.setRole(UserRole.USER.name());

        return user;
    }
}
