package com.uapp.similartrello.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RegistrationForm {
    @NotNull
    @Pattern(
            regexp = "^[a-zA-Z0-9]{1,100}$",
            message = "Username must be only the symbols of the alphabet and digits (length no more 100)"
    )
    private String username;

    @NotNull
    @Size(min = 3,max = 10, message="Password length must be more 3 an less 10")
    private String password;

    @NotNull
    @Size(min = 3,max = 10, message="Repeat password length must be more 3 an less 10")
    private String repeat;
}
