package com.uapp.similartrello.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    private Integer id;
    private String username;
    private String password;
    private boolean enabled;
    private String role;
}
