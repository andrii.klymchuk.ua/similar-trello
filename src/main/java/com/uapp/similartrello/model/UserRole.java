package com.uapp.similartrello.model;

public enum UserRole {
    USER, ADMIN
}

