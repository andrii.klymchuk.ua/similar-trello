package com.uapp.similartrello.repository;

import com.uapp.similartrello.model.User;

public interface UserRepository {

    User get(String username);

    void save(User user);
}
