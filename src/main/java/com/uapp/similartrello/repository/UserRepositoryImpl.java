package com.uapp.similartrello.repository;

import com.uapp.similartrello.exception.NotFoundException;
import com.uapp.similartrello.mapper.UserMapper;
import com.uapp.similartrello.model.User;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final String SQL_SELECT_BY_NAME = "SELECT id, username, password, enabled, role FROM users WHERE UPPER(username) LIKE UPPER(:username)";
    private final String SQL_INSERT = "INSERT INTO users (username, password, role) VALUES (:username, :password, :role)";
    private final String SQL_UPDATE = "UPDATE users SET username=:username, password=:password, role=:role WHERE id=:id";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserMapper userMapper;

    public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, UserMapper userMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userMapper = userMapper;
    }

    @Override
    public User get(String username) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("username", username);
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_BY_NAME, parameterSource, userMapper);
        } catch (Exception e) {
            throw new NotFoundException("User " + username + " not found");
        }
    }

    @Override
    public void save(User user) {
        if (user.getId() == null) {
            insert(user);
        } else {
            update(user);
        }
    }

    private void insert(User user) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_INSERT, getSource(user), keyHolder, new String[]{"id"});
        final int id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        user.setId(id);
    }

    private void update(User user) {
        jdbcTemplate.update(SQL_UPDATE, getSource(user));
    }

    private MapSqlParameterSource getSource(User user) {
        return new MapSqlParameterSource("id", user.getId())
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("role", user.getRole());
    }
}
