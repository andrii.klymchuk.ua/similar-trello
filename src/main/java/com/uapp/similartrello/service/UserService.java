package com.uapp.similartrello.service;

import com.uapp.similartrello.model.User;

public interface UserService {

    User get(String username);

    void save(User user);
}

