package com.uapp.similartrello.service;

import com.uapp.similartrello.exception.NotFoundException;
import com.uapp.similartrello.exception.ValidateException;
import com.uapp.similartrello.model.User;
import com.uapp.similartrello.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User get(String username) {
        return repository.get(username);
    }

    @Override
    public void save(User user) {
        if (user.isEnabled()) {
            repository.save(user);
        } else {
            saveNewUser(user);
        }
    }

    private void saveNewUser(User user) {
        try {
            repository.get(user.getUsername());
            throw new ValidateException("User by name " + user.getUsername() + "exist");
        } catch (NotFoundException e) {
            repository.save(user);
        }
    }
}

