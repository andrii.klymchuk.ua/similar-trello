package com.uapp.similartrello.service.logic;

import com.uapp.similartrello.exception.NotFoundException;
import com.uapp.similartrello.model.Group;
import com.uapp.similartrello.model.Task;
import com.uapp.similartrello.model.User;
import com.uapp.similartrello.service.GroupService;
import com.uapp.similartrello.service.TaskService;
import com.uapp.similartrello.service.UserService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class GroupingTaskServiceImpl implements GroupingTaskService {

    private final UserService userService;
    private final GroupService groupService;
    private final TaskService taskService;

    public GroupingTaskServiceImpl(UserService userService, GroupService groupService, TaskService taskService) {
        this.userService = userService;
        this.groupService = groupService;
        this.taskService = taskService;
    }

    @Override
    public Map<Group, List<Task>> groupingAll() {
        final List<Group> groups = groupService.getAll();
        final List<Task> tasks = taskService.getAll();

        return groupingTasks(groups, tasks);
    }

    @Override
    public Map<Group, List<Task>> grouping4User(String username) {
        try {
            final User user = userService.get(username);
            final List<Group> groups = groupService.getByUsers(List.of(user.getId()));
            final List<Integer> ids = groups.stream().map(Group::getId).toList();
            final List<Task> tasks = ids.isEmpty()
                    ? Collections.emptyList()
                    : taskService.getByGroups(groups.stream().map(Group::getId).toList());

            return groupingTasks(groups, tasks);
        } catch (NotFoundException e) {
            return Collections.emptyMap();
        }
    }

    @Override
    public Map<Group, List<Task>> getSortedGroupingTaskMap(Map<Group, List<Task>> map) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey(Comparator.comparing(Group::getPosition)))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> sortTaskList(e.getValue()),
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new)
                );
    }

    private Map<Group, List<Task>> groupingTasks(List<Group> groups, List<Task> tasks) {
        return groups.stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        group -> tasks.stream()
                                .filter(task -> task.getGroupId().equals(group.getId()))
                                .collect(Collectors.toCollection(ArrayList::new)),
                        (oldValue, newValue) -> oldValue)
                );

    }

    private List<Task> sortTaskList(List<Task> tasks) {
        return tasks.stream()
                .sorted(Comparator.comparing(Task::getPosition))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
