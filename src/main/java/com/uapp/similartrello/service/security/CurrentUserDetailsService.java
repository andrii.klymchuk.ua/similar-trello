package com.uapp.similartrello.service.security;

import com.uapp.similartrello.exception.NotFoundException;
import com.uapp.similartrello.model.User;
import com.uapp.similartrello.model.security.CurrentUser;
import com.uapp.similartrello.service.UserService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserDetailsService implements UserDetailsService {

    private final UserService service;

    public CurrentUserDetailsService(UserService service) {
        this.service = service;
    }

    @Override
    public CurrentUser loadUserByUsername(String username) throws NotFoundException {
        final User user = service.get(username);

        if (!user.isEnabled()) {
            throw new NotFoundException(String.format("User by name %s is lock", username));
        }

        return new CurrentUser(user);
    }
}
