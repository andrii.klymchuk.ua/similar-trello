CREATE TABLE users
(
    id       SERIAL       NOT NULL,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    enabled  boolean      NOT NULL DEFAULT TRUE,
    role     VARCHAR(10)  NOT NULL DEFAULT 'USER',

    PRIMARY KEY (id)
);