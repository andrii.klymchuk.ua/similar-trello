INSERT INTO users (id, username, password, role)
VALUES (1, 'admin', '$2a$10$MVrGqZBrhkFz9htzdZuVwujmVAHF7sVN0B8yi0qj6kQj7.j8s0ebO', 'ADMIN');

INSERT INTO users (id, username, password, role)
VALUES (2, 'user', '$2a$10$cC0hxIh4AhxrXWVbu3s1ZemTU.hlntkdzE.kgg0YV8yVkWlDTsZFS', 'USER');
