CREATE TABLE users
(
    id       SERIAL       NOT NULL,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    enabled  boolean      NOT NULL DEFAULT TRUE,
    role     VARCHAR(10)  NOT NULL DEFAULT 'USER',

    PRIMARY KEY (id)
);

CREATE TABLE groups
(
    id        SERIAL        NOT NULL,
    name      VARCHAR(255)  NOT NULL,
    position  INTEGER       NOT NULL,
    user_id   INTEGER       NOT NULL REFERENCES users (id) ON DELETE CASCADE,

    UNIQUE(user_id, name),
    UNIQUE(user_id, position),
    PRIMARY KEY (id)
);

CREATE TABLE task
(
    id            SERIAL        NOT NULL,
    name          VARCHAR(255)  NOT NULL,
    description   VARCHAR(255)  NOT NULL,
    date_create   DATE          NOT NULL,
    position      INTEGER       NOT NULL,
    group_id      INTEGER       NOT NULL REFERENCES groups (id) ON DELETE CASCADE,

    UNIQUE(group_id, position),
    UNIQUE(group_id, name, description),
    PRIMARY KEY (id)
);
